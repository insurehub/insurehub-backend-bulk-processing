FROM openjdk:11
MAINTAINER wellington.t.mpofu@gmail.com
ARG JAR_FILE=target/*.jar
COPY ${JAR_FILE} insurehub-backend-bulk-processing.jar
ENTRYPOINT ["java","-jar","/insurehub-backend-bulk-processing.jar"]