package com.kaributechs.insurehubbackendbulkprocessing.controllers;

import com.kaributechs.insurehubbackendbulkprocessing.models.ContactClients;
import com.kaributechs.insurehubbackendbulkprocessing.services.ContactClientsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1/contactclients")
public class ContactClientsController {

    @Autowired
    private ContactClientsService service;

    @PostMapping("/add")
    public ContactClients addClientInfo(@RequestBody ContactClients client){
        return service.addClientInfo(client);
    }

    @GetMapping("/{id}")
    public ContactClients getClientsInfoById(@PathVariable(value = "id") String clientid){
        return service.getClientsInfoById(clientid);
    }

    @GetMapping
    public List<ContactClients> getAllClientsInfo(){
        return service.getAllClientsInfo();
    }








}
