package com.kaributechs.insurehubbackendbulkprocessing.services;

import com.kaributechs.insurehubbackendbulkprocessing.models.ContactClients;
import com.kaributechs.insurehubbackendbulkprocessing.exceptions.ClientNotFoundException;
import com.kaributechs.insurehubbackendbulkprocessing.repositories.ContactClientsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;

@Service
public class ContactClientsService {
    @Autowired
    private ContactClientsRepository repository;

    public ContactClients addClientInfo(ContactClients client){
        return repository.save(client);
    }

    public ContactClients getClientsInfoById(String clientid){
        return repository.findById(clientid)
                .orElseThrow(()-> new ClientNotFoundException("Client not found with id" + clientid));
    }

    public List<ContactClients> getAllClientsInfo(){
        return repository.findAll();
    }


}

