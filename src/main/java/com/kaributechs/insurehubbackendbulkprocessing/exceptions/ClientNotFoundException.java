package com.kaributechs.insurehubbackendbulkprocessing.exceptions;

public class ClientNotFoundException extends RuntimeException{
    public ClientNotFoundException(String message){
        super(message);
    }
}
