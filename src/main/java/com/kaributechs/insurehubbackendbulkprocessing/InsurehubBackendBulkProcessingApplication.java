package com.kaributechs.insurehubbackendbulkprocessing;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class InsurehubBackendBulkProcessingApplication {

	public static void main(String[] args) {
		SpringApplication.run(InsurehubBackendBulkProcessingApplication.class, args);
	}

}
