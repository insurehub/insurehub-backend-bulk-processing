package com.kaributechs.insurehubbackendbulkprocessing.repositories;

import com.kaributechs.insurehubbackendbulkprocessing.models.ContactClients;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ContactClientsRepository extends JpaRepository<ContactClients,String > {
}
