package com.kaributechs.insurehubbackendbulkprocessing.configurations;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Contact;
import io.swagger.v3.oas.annotations.info.Info;
import io.swagger.v3.oas.annotations.servers.Server;

@OpenAPIDefinition(
        info = @Info(
                title = "Contact Bulk Clients",
                description = "" +
                        "This is an api documentation page for Contacting Bulk Clients developed by Kaributechs",
                contact = @Contact(
                        name = "Wellington Mpofu",
                        email = "wellington.t.mpofu@gmail.com"
                )
        ),
        servers = @Server(url = "http://insurehubbackend.southafricanorth.cloudapp.azure.com:7005/")
)
public class OpenApiConfig {
}
