package com.kaributechs.insurehubbackendbulkprocessing.models;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "ClientDetails")
public class ContactClients {
    @Id
    private String idNumber;
    private String firstName;
    private String lastName;
    private String claimType;
    private String messageBody;
    private String emailAddress;

    public ContactClients(){

    }

    public ContactClients(String idNumber, String firstName, String lastName, String claimType, String emailAddress, String messageBody) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.idNumber = idNumber;
        this.claimType = claimType;
        this.messageBody = messageBody;
        this.emailAddress = emailAddress;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getIdNumber() {
        return idNumber;
    }

    public void setIdNumber(String idNumber) {
        this.idNumber = idNumber;
    }

    public String getClaimType() {
        return claimType;
    }

    public void setClaimType(String claimType) {
        this.claimType = claimType;
    }

    public String getMessageBody() {
        return messageBody;
    }

    public void setMessageBody(String messageBody) {
        this.messageBody = messageBody;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }
}
